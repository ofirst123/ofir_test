import { Injectable } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  signUp(email:string,password:string){
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password);

  }
  updateProfile(user,name:string){
    user.updateProfile({displayName:name,photoURL:''})
  }


  login(email:string,password:string){
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password);
  }
  logout(){
    return this.fireBaseAuth.auth.signOut();
  }


  addUser(user,name: string,nickname:string){
    let uid = user.uid;
    let ref = this.db.database.ref('/');
    ref.child('users').child(uid).push({'name':name});
    ref.child('users').child(uid).push({'nickname':nickname});
    
  }

  user:Observable<firebase.User>;

  constructor(public fireBaseAuth:AngularFireAuth, public db:AngularFireDatabase) { 
    this.user = fireBaseAuth.authState;
  }
}