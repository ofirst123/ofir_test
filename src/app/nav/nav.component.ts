import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService} from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  errM

  toLogin(){
    this.router.navigate(['/login']);
  }

  toRegister(){
    this.router.navigate(['/register']);
  }

  toBooks(){
    this.router.navigate(['/books']);
  }

  toLogout(){
    this.authService.logout().
      then(value=>{this.router.navigate(['/login'])
    })
    .catch(err=>{
      this.errM = err;
      console.log("blalalalalal");
    })
  }
 

  constructor(public authService:AuthService,public router:Router) { }

  ngOnInit() {
  }

}
