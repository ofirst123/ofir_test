import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService} from '../auth.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { ItemsService } from '../items.service';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

books = [];

name;
  newBook:string;
  newAuther:string;
  read:boolean = false;

  toLogin(){
    this.router.navigate(['/login']);
  }

  toRegister(){
    this.router.navigate(['/register']);
  }

  showText($event){
    this.name=$event;
    
  }


  constructor(public authService:AuthService, public itemsService:ItemsService, public db:AngularFireDatabase, public router:Router) { }

  addItem(){
    this.itemsService.addItem(this.newBook,this.newAuther,this.read);
    this.newBook='';
    this.newAuther='';
    

  }

  ngOnInit() {
    this.authService.user.subscribe(user=> {
      this.db.list('/users/'+user.uid+'/books/').snapshotChanges().subscribe(
        books=>{
          this.books = [];
          books.forEach(
            book =>{
              let y = book.payload.toJSON();
              y["$key"] = book.key;
              this.books.push(y);
            }
          )
        }
      )

    })


  }

}