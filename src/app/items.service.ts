import { Injectable } from '@angular/core';
import { AuthService} from './auth.service';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  addItem(newBook,newAuther,read){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/books/').push({'name':newBook,'auther':newAuther,'read':read});
    })
  }
  delete(key){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/books/').remove(key);
    })
  }
  changeRead(key,read:boolean){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/books/').update(key,{'read':read});
    })

  }

  save(key,name:string, auther:string){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/books/').update(key,{'name':name, 'auther':auther});
    })
  }

  constructor(public authService:AuthService, public db:AngularFireDatabase) { }
}
