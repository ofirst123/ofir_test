import { Component, OnInit } from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { AuthService} from '../auth.service';
import {Router} from '@angular/router';




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  email1: string;
  password: string;
  password2: string;
  name: string;
  errM;
  nickname:string;

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  signUp(){
    if (this.password !== this.password2){
      this.errM= 'The two password does not match'}
    else{
   return this.authService.signUp(this.email1,this.password)
      .then(value =>{
        this.authService.updateProfile(value.user,this.nickname);
        this.authService.addUser(value.user,this.name,this.nickname);
      })

        .then(value=>{
          this.router.navigate(['/books']);
        })
          .catch(err=>{
            this.errM = err;
            console.log(err);
            console.log(this.authService.user);
          })
    
  
  }}

}

