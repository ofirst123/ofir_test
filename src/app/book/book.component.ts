import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';
import { AuthService} from '../auth.service';
import { ItemsService } from '../items.service';
import { noop } from 'rxjs';
import { ChangeDetectorStatus } from '@angular/core/src/change_detection/constants';


@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClick=new EventEmitter<any>();
  

  name;
  id;
  
  auther;
  tempName;
  tempAuther;
  key;
  read:boolean;

  showButton = false;
  showEditField=false;

  
 

  delete(){
    this.itemsService.delete(this.key);
  }
 changeRead(){
  console.log(!this.read);

  this.itemsService.changeRead(this.key,!this.read);

 }
  showEdit(){
    this.showEditField= true;
   this.tempName=this.name;
   this.tempAuther=this.auther;
  
 }

 save(){
  this.itemsService.save(this.key,this.name,this.auther);
  this.showEditField=false;
}

 cancel(){
   this.name=this.tempName;
   this.auther=this.tempAuther;
   this.showEditField=false;
   
  }


  over(){
    this.showButton = true;
  }
  notOver(){
    this.showButton = false;
  }
  constructor(public authService:AuthService, public itemsService:ItemsService, public db:AngularFireDatabase) { }

  ngOnInit() {
    this.name = this.data.name;
    this.auther = this.data.auther;
    this.key = this.data.$key
    this.read = this.data.read;


  }

}