import { Component, OnInit } from '@angular/core';
import { AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  email1: string;
  password: string;
  errM;

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  login(){
    if(this.email1==null || this.password==null)
      this.errM= "you must input email and password"
    return this.authService.login(this.email1,this.password)
      .then(value=>{
        this.router.navigate(['/books']);
        
      })
      .catch(err=>{
        this.errM = err;
        console.log("blalalalalal");
      })
  }

}